function deleteCookieProp(propName) {
  document.cookie = `${propName}= ; expires = Thu, 01 Jan 1970 00:00:00 GM`;
}

function getTokenFromCookie(propName) {
  const token = document.cookie.split(';').find((el) => el.indexOf(propName) !== -1);
  return token ? token.match(/(?<=token=).+$/)[0] : false;
}

function tip(text, type) {
  const tipEl = document.createElement('div');
  tipEl.classList.add('tip', 'tip--slideIn', `tip--${type}`);
  tipEl.textContent = text;
  tipEl.addEventListener('animationend', () => {
    setTimeout(() => {
      tipEl.classList.remove('tip--slideIn');
      tipEl.classList.add('tip--slideOut');
      tipEl.addEventListener('animationend', () => {
        tipEl.remove();
      });
    }, 3000);
  }, { once: true });

  document.body.append(tipEl);
}

function handleDeleteProfile() {
  const deleteBtn = document.querySelector('.delete-profile-btn');

  if (!deleteBtn) {
    return;
  }

  deleteBtn.addEventListener('click', async () => {
    try {
      const response = await fetch('/api/users/me', {
        method: 'DELETE',
        headers: {
          authorization: `Bearer ${getTokenFromCookie('token')}`,
        },
      });
      if (!response.ok) {
        throw await response.json();
      } else {
        deleteCookieProp('token');
        window.location.href = `${window.location.protocol}//${window.location.host}`;
      }
    } catch (error) {
      tip(error.message, 'danger');
    }
  });
}

function handleLogout() {
  const logoutBtn = document.querySelector('.logout-btn');

  if (!logoutBtn) {
    return;
  }

  logoutBtn.addEventListener('click', () => {
    deleteCookieProp('token');
    window.location.href = `${window.location.protocol}//${window.location.host}`;
  });
}

function modalMe() {
  const modalButtons = document.querySelectorAll('[data-modal-id]');
  const modals = document.querySelectorAll('.modal');

  modalButtons.forEach((btn) => {
    btn.addEventListener('click', (e) => {
      e.preventDefault();
      const { modalId } = e.currentTarget.dataset;
      const modal = document.querySelector(`#${modalId}`);
      modal.classList.add('modal--active');
    });
  });

  modals.forEach((modal) => {
    modal.addEventListener('click', (e) => {
      if (e.target === e.currentTarget || e.target.dataset.modalClose) {
        e.currentTarget.classList.remove('modal--active');
      }
    });
  });
}

function handleRegisterForm() {
  const form = document.querySelector('#register-form');

  if (!form) {
    return;
  }

  form.addEventListener('submit', async (e) => {
    e.preventDefault();
    const formData = new FormData(e.currentTarget);

    try {
      const response = await fetch('/api/auth/register', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          username: formData.get('username'),
          password: formData.get('password'),
        }),
      });
      if (!response.ok) {
        throw await response.json();
      } else {
        tip('Now you can login', 'ok');
        document.getElementById('register-modal').classList.remove('modal--active');
        form.reset();
      }
    } catch (error) {
      tip(error.message, 'danger');
    }
  });
}

function handleLoginForm() {
  const form = document.querySelector('#login-form');

  if (!form) {
    return;
  }

  form.addEventListener('submit', async (e) => {
    e.preventDefault();
    const formData = new FormData(e.currentTarget);

    try {
      const response = await fetch('/api/auth/login', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          username: formData.get('username'),
          password: formData.get('password'),
        }),
      });
      if (!response.ok) {
        throw await response.json();
      } else {
        const res = await response.json();
        tip('Now you can login', 'ok');
        document.cookie = `token=${res.jwt_token}`;
        window.location.href = `${window.location.protocol}//${window.location.host}/notes`;
      }
    } catch (error) {
      tip(error.message, 'danger');
    }
  });
}

function handleCreateNoteForm() {
  const form = document.querySelector('#createNote-form');

  if (!form) {
    return;
  }

  form.addEventListener('submit', async (e) => {
    e.preventDefault();
    const formData = new FormData(e.currentTarget);

    try {
      const response = await fetch('/api/notes/', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${getTokenFromCookie('token')}`,
        },
        body: JSON.stringify({
          text: formData.get('text'),
        }),
      });
      if (!response.ok) {
        throw await response.json();
      } else {
        tip('Note created', 'ok');
        window.location.reload();
      }
    } catch (error) {
      tip(error.message, 'danger');
    }
  });
}

function handlePasswordForm() {
  const form = document.querySelector('#password-form');

  if (!form) {
    return;
  }

  form.addEventListener('submit', async (e) => {
    e.preventDefault();
    const formData = new FormData(e.currentTarget);

    try {
      const response = await fetch('/api/users/me', {
        method: 'PATCH',
        headers: {
          authorization: `Bearer ${getTokenFromCookie('token')}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          oldPassword: formData.get('oldPassword'),
          newPassword: formData.get('newPassword'),
        }),
      });
      if (!response.ok) {
        throw await response.json();
      } else {
        tip('Password was successfully changed', 'ok');
        document.getElementById('password-modal').classList.remove('modal--active');
        form.reset();
      }
    } catch (error) {
      tip(error.message, 'danger');
    }
  });
}

async function deleteNote(id) {
  try {
    const response = await fetch(`/api/notes/${id}`, {
      headers: {
        authorization: `Bearer ${getTokenFromCookie('token')}`,
      },
      method: 'DELETE',
    });
    if (!response.ok) {
      throw await response.json();
    } else {
      document.querySelector(`[data-note-id="${id}"]`).remove();
      tip('Note was deleted', 'ok');
    }
  } catch (error) {
    tip(error.message, 'danger');
  }
}

async function completeNote(id) {
  try {
    const response = await fetch(`/api/notes/${id}`, {
      headers: {
        authorization: `Bearer ${getTokenFromCookie('token')}`,
      },
      method: 'PATCH',
    });
    if (!response.ok) {
      throw await response.json();
    }
  } catch (error) {
    tip(error.message, 'danger');
  }
}

async function updateNote(id) {
  try {
    const text = document.querySelector(`[data-note-id="${id}"] .note-text`).value;
    const response = await fetch(`/api/notes/${id}`, {
      headers: {
        authorization: `Bearer ${getTokenFromCookie('token')}`,
        'Content-Type': 'application/json',
      },
      method: 'PUT',
      body: JSON.stringify({ text }),
    });
    if (!response.ok) {
      throw await response.json();
    } else {
      tip('Note was updated', 'ok');
    }
  } catch (error) {
    tip(error.message, 'danger');
  }
}

function handleNote() {
  const notes = document.querySelectorAll('.notes__item');

  notes.forEach((note) => {
    const id = note.dataset.noteId;
    note.addEventListener('click', (e) => {
      if (e.target.classList.contains('.note__completed') || e.target.closest('.note__completed')) {
        completeNote(id);
      }

      if (e.target.classList.contains('.save-note') || e.target.closest('.save-note')) {
        updateNote(id);
      }

      if (e.target.classList.contains('.delete-note') || e.target.closest('.delete-note')) {
        deleteNote(id);
      }
    });
  });
}

document.addEventListener('DOMContentLoaded', () => {
  handleRegisterForm();
  handleLoginForm();
  modalMe();
  handleCreateNoteForm();
  handleNote();
  handlePasswordForm();
  handleLogout();
  handleDeleteProfile();
});
