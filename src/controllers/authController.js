const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');
const User = require('../models/userModel');

dotenv.config();

const register = async (req, res) => {
  const { username, password } = req.body;

  if (await User.findOne({ username })) {
    res.status(400).json({ message: `User with username: ${username} already exists` });
    return;
  }

  const user = new User({ username, password: password && await bcrypt.hash(password, 10) });

  user.save()
    .then(() => {
      res.status(200).json({ message: 'Success' });
    })
    .catch((err) => {
      res.status(400).json({ message: err.message });
    });
};

const login = async (req, res) => {
  const { username, password } = req.body;
  const user = await User.findOne({ username });

  if (user && await bcrypt.compare(String(password), String(user.password))) {
    const payload = { username: user.username, userId: user._id };
    const jwtToken = jwt.sign(payload, process.env.TOKEN_SECRET);

    return res.json({ message: 'Success', jwt_token: jwtToken });
  }

  return res.status(400).json({ message: 'Invalid login or password' });
};

module.exports = {
  register,
  login,
};
