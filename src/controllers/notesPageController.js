const jwt = require('jsonwebtoken');
const axios = require('axios');
const User = require('../models/userModel');
const getTokenFromCookie = require('../middleware/getTokenFromCookieMiddleware');

const getNotes = async (req, res, next) => {
  const token = getTokenFromCookie(req.headers.cookie, 'token=');

  if (!token) {
    return res.status(301).redirect('/');
  }

  const userData = jwt.verify(token, process.env.TOKEN_SECRET);
  const user = await User.findById(userData.userId);

  if (!user) {
    return res.status(301).redirect('/');
  }

  axios.get(`${req.protocol}://${req.get('host')}/api/notes`, {
    headers: { Authorization: `Bearer ${token}` },
  })
    .then((result) => res.render('notes', {
      notes: result.data.notes,
      username: user.username,
      id: user._id,
      createdDate: user.createdAt,
    }))
    .catch(next);
};

module.exports = {
  getNotes,
};
