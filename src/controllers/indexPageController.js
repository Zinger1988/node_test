const jwt = require('jsonwebtoken');
const User = require('../models/userModel');
const getTokenFromCookie = require('../middleware/getTokenFromCookieMiddleware');

module.exports = (req, res) => {
  const token = getTokenFromCookie(req.headers.cookie, 'token=');

  if (!token) {
    res.render('index');
    return;
  }

  const userData = jwt.verify(token, process.env.TOKEN_SECRET);

  if (User.findById(userData.userId)) {
    return res.status(301).redirect('/notes');
  }

  res.render('index');
};
