module.exports = (cookies, tokenPropName) => {
  const token = cookies.split(';').find((el) => el.indexOf(tokenPropName) !== -1);
  return token ? token.match(/(?<=token=).+$/)[0] : false;
};
