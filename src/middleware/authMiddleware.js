const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');

dotenv.config();

const authMiddleware = (req, res, next) => {
  const { authorization } = req.headers;

  if (!authorization) {
    return res.status(400).json({ message: 'Authorization header required' });
  }

  const [, token] = authorization.split(' ');

  if (!token) {
    return res.status(400).json({ message: 'Authorization token required' });
  }

  try {
    const payload = jwt.verify(token, process.env.TOKEN_SECRET);
    req.user = {
      userId: payload.userId,
      username: payload.username,
    };
    next();
  } catch (err) {
    return res.status(400).json({ message: err.message });
  }
};

module.exports = authMiddleware;
