const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const dotenv = require('dotenv');
const fs = require('fs');
const path = require('path');
const notesRoutes = require('./routers/notesRoutes');
const authRoutes = require('./routers/authRoutes');
const usersRoutes = require('./routers/usersRoutes');
const notesPageRoutes = require('./routers/notesPageRoutes');
const indexPageRoutes = require('./routers/indexPageRoutes');
const errorHandlerMiddleware = require('./middleware/errorHandlerMiddleware');

const app = express();
const accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), { flags: 'a' });
dotenv.config();

mongoose.connect(process.env.DB_CONN)
  .then(() => app.listen(process.env.PORT))
  .catch((err) => console.error(`Error on server startup: ${err.message}`));

app.set('view engine', 'ejs');
app.set('views', './src/views');

app.use(express.static('public'));
app.use(express.json());
app.use(morgan('tiny', { stream: accessLogStream }));

app.use(indexPageRoutes);
app.use('/notes', notesPageRoutes);
app.use('/api/notes', notesRoutes);
app.use('/api/auth', authRoutes);
app.use('/api/users/me', usersRoutes);

app.use(errorHandlerMiddleware);
