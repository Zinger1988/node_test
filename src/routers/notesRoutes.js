const express = require('express');
const authMiddleware = require('../middleware/authMiddleware');
const notesController = require('../controllers/notesController');

const router = express.Router();

router.get('/', authMiddleware, notesController.getAllNotes);
router.post('/', authMiddleware, notesController.createNote);
router.get('/:id', authMiddleware, notesController.getNote);
router.put('/:id', authMiddleware, notesController.updateNote);
router.patch('/:id', authMiddleware, notesController.markNoteAsCompleted);
router.delete('/:id', authMiddleware, notesController.deleteNote);

module.exports = router;
