const express = require('express');

const router = express.Router();
const notesPageController = require('../controllers/notesPageController');

router.get('/', notesPageController.getNotes);

module.exports = router;
