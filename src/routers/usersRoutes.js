const express = require('express');
const authMiddleware = require('../middleware/authMiddleware');
const usersController = require('../controllers/usersController');

const router = express.Router();

router.get('/', authMiddleware, usersController.getUserInfo);
router.delete('/', authMiddleware, usersController.deleteUser);
router.patch('/', authMiddleware, usersController.changeUserPassword);

module.exports = router;
