const express = require('express');

const router = express.Router();
const indexPageController = require('../controllers/indexPageController');

router.get('/', indexPageController);

module.exports = router;
